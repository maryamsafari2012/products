<?php

namespace App\Http\Controllers\API;

use App\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;


class ProductController extends Controller
{
    public $successStatus = 200;

    public function create(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['is_active'] = 0;
        $input['user_id'] = $id;
        $products = Products::create($input);
        return response()->json(['success' => $products->name], $this->successStatus);
    }

    public function getProduct($id)
    {
        $products = Products::where('is_active', 0)
            ->where('user_id', $id)
            ->get();

        return response()->json(['success' => $products], $this->successStatus);
    }

}