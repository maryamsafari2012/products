<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{

    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'is_active', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function productPicture()
    {
        return $this->hasOne('App\ProductsPicture');
    }

    public function productPrice()
    {
        return $this->hasOne('App\ProductsPrice');
    }
}
