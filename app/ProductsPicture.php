<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ProductsPicture extends Model
{

    protected $table = 'product_picture';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'path',
    ];

    public function products()
    {
        return $this->belongsTo('App\Products');
    }
}
