<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ProductsPrice extends Model
{

    protected $table = 'product_price';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'price',
    ];

    public function products()
    {
        return $this->belongsTo('App\Products');
    }
}
