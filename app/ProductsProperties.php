<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ProductsProperties extends Model
{

    protected $table = 'product_properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'properties_id',
    ];

    public function products()
    {
        return $this->belongsTo('App\Products');
    }

    public function properties()
    {
        return $this->belongsTo('App\Properties');
    }
}
