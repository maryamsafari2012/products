# Product Project
##Installation
1. Clone the project.
2. First You have to install `composer`.
3. Then cd to the root directory of the project.
4. `composer install`.
5. Make sure that `artisan` is available.
6. Create database named as `product`.
7. Create .env file and save database information.
8. Migrate up the project. `php artisan migrate`.
9. Run the server by `php artisan serve`.
10. Create user

##API Guide
POST `/api/register` 

POST `/api/login`

GET `/users`

GET `/users/{id}`

POST `/users/{id}/products`

GET `/users/{id}/products`


You can test it by Postman. 


