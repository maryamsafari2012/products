<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//user
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
});

Route::get('/users', 'API\UserController@getUsers');
Route::get('/users/{id}', 'API\UserController@getUser');



Route::post('/users/{id}/products', 'API\ProductController@create');
Route::get('/users/{id}/products', 'API\ProductController@getProduct');